<?php
// LOAD CLASSES AND COMPOSER
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/DecreaseCPAClass.php';
require __DIR__ . '/IncreaseCPAClass.php';

// SYSTEM USER ACCESS TOKEN (long string)
const ACCESS_TOKEN = '';
// DEVELOPER APP SECRET KEY (32 string)
const APP_SECRET = '';
// DEVELOPER APP APP ID (15 string)
const APP_ID = '';
// ADD ACCOUNT ID, FOR ACCOUNT YOU WANT DO DO CHANGES IN (16 string)
const AD_ACCOUNT_ID = 'act_' . ''; // change second part, leave 'act_' as is otherwise FB can't find account!

/**
 * BEGIN SCRIPTS EXECUTION
 */

echo "**************** DECREASE CPA STARTED ********************" . PHP_EOL . PHP_EOL;

// decrease CPA rules
(new DecreaseCPAClass())->execute();

echo "************ BEGIN CREATION  DECREASE CPA ****************" . PHP_EOL;

// increase CPA rules
(new IncreaseCPAClass())->execute();

echo "****************** STOPPING SERVICE **********************";